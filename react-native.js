import React, { Component } from "react";
import {
  Image,
  Picker,
  View,
  TouchableNativeFeedback,
  TouchableOpacity,
  Platform,
  Dimensions,
  AsyncStorage,
  AppState,
} from "react-native";
import styled from "styled-components";
import LinearGradient from "react-native-linear-gradient";
import ReactNativeHaptic from "./Haptic";
import DropdownAlert from "react-native-dropdownalert";
import DropdownFlags from "./DropdownFlags";

const isAndroid = Platform.OS === "android";
const Touchable = isAndroid ? TouchableNativeFeedback : TouchableOpacity;

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

const isPad = aspectRatio < 1.6;

export default class Calculator extends Component<{}> {
  static navigatorButtons = {
    rightButtons: [
      {
        title: "Kurzy",
        icon: require("./assets/rates.png"),
        iconColor: "#fff",
        id: "rates",
        showAsAction: "always",
        buttonColor: "#fff",
        buttonFontSize: 15,
        buttonFontWeight: "500",
      },
    ],
  };

  constructor(props) {
    super(props);
    this.state = {
      sum: "100",
      currencyFromName: "EUR",
      currencyFromRate: 1,
      currencyToName: "USD",
      currencyToRate: 1,
      currencyRates: {},
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    AppState.addEventListener("change", (state) => {
      if (state === "background") {
        AsyncStorage.setItem("@LocalStore:state", JSON.stringify(this.state));
      }
    });
  }

  onNavigatorEvent(event) {
    if (event.type === "NavBarButtonPress") {
      if (event.id === "rates") {
        this.props.navigator.push({
          title: "Kurzy mien",
          screen: "RatesPage",
          backButtonTitle: "",
        });
      }
    }
  }

  componentDidMount() {
    setTimeout(() => {
      AsyncStorage.getItem("@LocalStore:tutorialFinished").then((data) => {
        if (data === null || data !== "true") {
          this.props.navigator.resetTo({
            screen: "TutorialPage",
            animated: true,
            animationType: "fade",
            navigatorStyle: {
              navBarTransparent: true,
            },
          });
          this.props.navigator.toggleNavBar({
            to: "hidden",
            animated: true,
          });
        }
      });
    }, 500);

    AsyncStorage.getItem("@LocalStore:state").then((data) => {
      if (data !== null) {
        this.setState(JSON.parse(data));
      }
    });
    ReactNativeHaptic.prepare();
    this.timestamp = new Date().getTime();
    AsyncStorage.getItem("@LocalStore:rates").then((data) => {
      if (data !== null) {
        data = JSON.parse(data);
        this.setState({
          currencyRates: data,
          currencyToRate: data["rate"][this.state.currencyToName],
        });
        AsyncStorage.getItem("@LocalStore:lastFetched").then((lastFetched) => {
          isNaN(lastFetched) ? this.fetchRates() : null;
          if (lastFetched !== null) {
            if (this.timestamp - Number(lastFetched) > 30 * 60 * 1000) {
              this.fetchRates();
            }
          } else {
            this.fetchRates();
          }
        });
      } else {
        this.fetchRates();
      }
    });
  }

  fetchRates() {
    fetch("https://api.frankfurter.app/latest")
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(
          {
            currencyRates: responseJson,
            currencyToRate: responseJson["rates"][this.state.currencyToName],
          },
          function () {
            AsyncStorage.setItem(
              "@LocalStore:rates",
              JSON.stringify(responseJson)
            );
            AsyncStorage.setItem(
              "@LocalStore:lastFetched",
              String(this.timestamp)
            );
          }
        );
      })
      .catch((error) => {
        console.error(error);
      });
  }

  decimalPlaces(num) {
    var match = ("" + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
    if (!match) {
      return 0;
    }
    return Math.max(
      0,
      // Number of digits right of decimal point.
      (match[1] ? match[1].length : 0) -
        // Adjust for scientific notation.
        (match[2] ? +match[2] : 0)
    );
  }

  formatInput(number) {
    number = String(number);
    return number.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");
  }

  result() {
    return this.formatInput(
      Math.round(
        this.state.sum *
          (this.state.currencyToRate / this.state.currencyFromRate) *
          100
      ) / 100
    );
  }

  pickerList() {
    if (this.state.currencyRates.rates) {
      return this.state.currencyRates.rates.map((item, index) => {
        return <Picker.Item key={index} value={item.name} label={item.name} />;
      });
    }
  }

  flags = {
    EUR: require("./assets/flags/EUR.png"),
    USD: require("./assets/flags/USD.png"),
    JPY: require("./assets/flags/JPY.png"),
    BGN: require("./assets/flags/BGN.png"),
    CZK: require("./assets/flags/CZK.png"),
    DKK: require("./assets/flags/DKK.png"),
    GBP: require("./assets/flags/GBP.png"),
    HUF: require("./assets/flags/HUF.png"),
    PLN: require("./assets/flags/PLN.png"),
    RON: require("./assets/flags/RON.png"),
    SEK: require("./assets/flags/SEK.png"),
    CHF: require("./assets/flags/CHF.png"),
    ISK: require("./assets/flags/ISK.png"),
    NOK: require("./assets/flags/NOK.png"),
    HRK: require("./assets/flags/HRK.png"),
    RUB: require("./assets/flags/RUB.png"),
    TRY: require("./assets/flags/TRY.png"),
    AUD: require("./assets/flags/AUD.png"),
    BRL: require("./assets/flags/BRL.png"),
    CAD: require("./assets/flags/CAD.png"),
    CNY: require("./assets/flags/CNY.png"),
    HKD: require("./assets/flags/HKD.png"),
    IDR: require("./assets/flags/IDR.png"),
    ILS: require("./assets/flags/ILS.png"),
    INR: require("./assets/flags/INR.png"),
    KRW: require("./assets/flags/KRW.png"),
    MXN: require("./assets/flags/MXN.png"),
    MYR: require("./assets/flags/MYR.png"),
    NZD: require("./assets/flags/NZD.png"),
    PHP: require("./assets/flags/PHP.png"),
    SGD: require("./assets/flags/SGD.png"),
    THB: require("./assets/flags/THB.png"),
    ZAR: require("./assets/flags/ZAR.png"),
  };

  switchCurrencies = () => {
    ReactNativeHaptic.generate("impact");
    let a = this.state.currencyToName;
    let b = this.state.currencyToRate;
    this.setState({
      currencyToName: this.state.currencyFromName,
      currencyToRate: this.state.currencyFromRate,
      currencyFromName: a,
      currencyFromRate: b,
      sum: this.result().replace(/ /g, ""),
    });
  };

  deleteInput = () => {
    ReactNativeHaptic.generate("impact");
    this.setState({ sum: "0" });
    this.showError("Použite klávesnicu nižšie.");
  };

  render() {
    const flagFrom = this.flags[this.state.currencyFromName];
    const flagTo = this.flags[this.state.currencyToName];
    const ratesDate = new Date(
      Date.parse(this.state.currencyRates.date)
    ).toLocaleDateString();
    return (
      <Container>
        <LinearGradient
          style={{ padding: isPad ? 8 : width * 0.05 }}
          colors={["#1f55ff", "#58acea"]}
        >
          <CurrencyRow>
            <FlagCol>
              <FlagContainer>
                <DropdownFlags
                  options={
                    this.state.currencyRates.rates && [
                      {
                        name: "EUR",
                        rate: 1,
                      },
                      ...this.state.currencyRates.rates,
                    ]
                  }
                  selectedValue={(currencySymbol) =>
                    this.handleDropdownFlagSelector("from", currencySymbol)
                  }
                  flags={this.flags}
                  longNames={this.longNames}
                  renderItem={({ item }) => (
                    <FlagSelectContainer>
                      <FlagSelectImg source={this.flags[option.name]} />
                      <FlagSelectSymbol>{item.name}</FlagSelectSymbol>
                    </FlagSelectContainer>
                  )}
                >
                  <FlagImg source={flagFrom} />
                  <CurrencyCode>{this.state.currencyFromName}</CurrencyCode>
                  <DropdownArrow source={require("./assets/arrow-down.png")} />
                </DropdownFlags>
              </FlagContainer>
            </FlagCol>
            <InputCol>
              <Touchable onPress={this.deleteInput}>
                <CurrencyNumber
                  style={{
                    fontSize: this.getBigDigitsSize(
                      this.formatInput(this.state.sum)
                    ),
                    fontWeight: "700",
                  }}
                >
                  {this.formatInput(this.state.sum)}
                </CurrencyNumber>
              </Touchable>
            </InputCol>
          </CurrencyRow>
          <DividerRow>
            <HorizontalLine />
            <Touchable
              accessibilityComponentType="button"
              onPress={this.switchCurrencies}
            >
              <SwapIcon>
                <Image
                  style={{ width: isPad ? 20 : 26, height: isPad ? 20 : 26 }}
                  source={require("./assets/arrows.png")}
                />
              </SwapIcon>
            </Touchable>
          </DividerRow>
          <CurrencyRow>
            <FlagCol>
              <FlagContainer>
                <DropdownFlags
                  options={
                    this.state.currencyRates.rates && [
                      {
                        name: "EUR",
                        rate: 1,
                      },
                      ...this.state.currencyRates.rates,
                    ]
                  }
                  selectedValue={(currencySymbol) =>
                    this.handleDropdownFlagSelector("to", currencySymbol)
                  }
                  flags={this.flags}
                  longNames={this.longNames}
                  renderItem={({ item }) => (
                    <FlagSelectContainer>
                      <FlagSelectImg source={this.flags[option.name]} />
                      <FlagSelectSymbol>{item.name}</FlagSelectSymbol>
                    </FlagSelectContainer>
                  )}
                >
                  <FlagImg source={flagTo} />
                  <CurrencyCode>{this.state.currencyToName}</CurrencyCode>
                  <DropdownArrow source={require("./assets/arrow-down.png")} />
                </DropdownFlags>
              </FlagContainer>
            </FlagCol>
            <InputCol>
              <CurrencyNumber
                style={{ fontSize: this.getBigDigitsSize(this.result()) }}
              >
                {this.result()}
              </CurrencyNumber>
            </InputCol>
          </CurrencyRow>
          {!isPad && (
            <DateText>Referenčné výmenné kurzy ECB ku dňu {ratesDate}</DateText>
          )}
        </LinearGradient>
        {isIphoneX() && (
          <PickerRow>
            <ArrowIcon source={require("./assets/arrow.png")} />
            <PickerColumn>
              <Picker
                itemStyle={{ height: 150 }}
                selectedValue={this.state.currencyFromName}
                onValueChange={(itemValue) =>
                  this.setState({
                    currencyFromName: itemValue,
                    currencyFromRate:
                      itemValue === "EUR"
                        ? 1
                        : this.state.currencyRates.rate[itemValue],
                  })
                }
              >
                <Picker.Item key="EUR" value="EUR" label="EUR" />
                {this.pickerList()}
              </Picker>
            </PickerColumn>
            <PickerColumn>
              <Picker
                itemStyle={{ height: 150 }}
                selectedValue={this.state.currencyToName}
                onValueChange={(itemValue) =>
                  this.setState({
                    currencyToName: itemValue,
                    currencyToRate:
                      itemValue === "EUR"
                        ? 1
                        : this.state.currencyRates.rate[itemValue],
                  })
                }
              >
                <Picker.Item key="EUR" value="EUR" label="EUR" />
                {this.pickerList()}
              </Picker>
            </PickerColumn>
          </PickerRow>
        )}
        <View
          style={{
            paddingBottom: isIphoneX() ? 0 : 10,
            paddingTop: isIphoneX() ? 0 : 10,
            flex: 1,
          }}
        >
          {this._renderKeypadButtons()}
        </View>

        <DropdownAlert
          closeInterval={2000}
          errorColor="rgba(10, 157, 255, 0.6)"
          ref={(ref) => (this.dropdown = ref)}
        />
      </Container>
    );
  }

  handleDropdownFlagSelector(direction, currencySymbol) {
    let result =
      direction === "to"
        ? {
            currencyToName: currencySymbol,
            currencyToRate:
              currencySymbol === "EUR"
                ? 1
                : this.state.currencyRates.rate[currencySymbol],
          }
        : {
            currencyFromName: currencySymbol,
            currencyFromRate:
              currencySymbol === "EUR"
                ? 1
                : this.state.currencyRates.rate[currencySymbol],
          };
    this.setState(result);
    ReactNativeHaptic.generate("selection");
  }

  _renderKeypadButtons() {
    let views = [];

    for (var r = 0; r < this.keypadButtons.length; r++) {
      let row = this.keypadButtons[r];

      let keypadRow = [];
      for (var i = 0; i < row.length; i++) {
        let keypad = row[i];
        if (keypad !== "DEL") {
          keypadRow.push(
            <KeypadTouchable
              key={r + "-" + i}
              onPress={() => {
                this.handleKeypadEvent(keypad);
                ReactNativeHaptic.generate("selection");
              }}
            >
              <KeypadButton>
                <KeypadText>{keypad}</KeypadText>
              </KeypadButton>
            </KeypadTouchable>
          );
        } else {
          keypadRow.push(
            <KeypadTouchable
              key={r + "-" + i}
              onPressOut={() => {
                ReactNativeHaptic.generate("selection");
              }}
              onLongPress={() => {
                this.setState({ sum: "0" });
              }}
            >
              <KeypadButton>
                <KeypadBackspace source={require("./assets/backspace.png")} />
              </KeypadButton>
            </KeypadTouchable>
          );
        }
      }

      views.push(<KeypadRow key={"row-" + r}>{keypadRow}</KeypadRow>);
    }

    return views;
  }

  keypadButtons = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [".", 0, "DEL"],
  ];

  getBigDigitsSize(num) {
    const baseSize = isPad ? 30 : 40;
    const scaleFactorSize = isPad ? 8 : isAndroid ? 7.5 : 6;
    var length = num.replace(/ /g, "").length;
    if (length < 6) {
      return baseSize;
    } else {
      return baseSize - (length - scaleFactorSize) * 3;
    }
  }

  handleKeypadEvent(event) {
    let { sum } = this.state;
    switch (event) {
      case "DEL":
        if (sum.length === 1) {
          this.setState({
            sum: "0",
          });
        } else {
          this.setState({
            sum: sum.substring(0, sum.length - 1),
          });
        }

        break;
      case ".":
        if (sum.indexOf(".") === -1) {
          this.setState({
            sum: sum + event,
          });
        }
        break;
      default:
        if (this.decimalPlaces(sum) < 3) {
          if (sum <= 999999999) {
            if (sum != "0") {
              this.setState({
                sum: sum + event,
              });
            } else if (sum == "0") {
              this.setState({
                sum: "" + event,
              });
            }
          } else {
            this.showError("Je možné zadať maximálne 10 číslic");
          }
        } else {
          this.showError("Je možné zadať maximálne 3 desatinné miesta");
        }
    }
  }

  showError = (error) => {
    if (error) {
      this.dropdown.alertWithType("error", "", error);
    }
  };

  longNames = {
    EUR: "Euro",
    USD: "Americký dolár",
    JPY: "Japonský jen",
    BGN: "Bulharský lev",
    CZK: "Česká koruna",
    DKK: "Dánska koruna",
    GBP: "Anglická libra",
    HUF: "Maďarský forint",
    PLN: "Poľský zlotý",
    RON: "Rumunské leu",
    SEK: "Švédska koruna",
    CHF: "Švajčiarsky frank",
    ISK: "Islandská koruna",
    NOK: "Nórska koruna",
    HRK: "Chorvátska kuna",
    RUB: "Ruský rubeľ",
    TRY: "Turecká líra",
    AUD: "Austrálsky dolár",
    BRL: "Brazílsky reál",
    CAD: "Kanadský dolár",
    CNY: "Čínsky juan",
    HKD: "Honkongský dolár",
    IDR: "Indonézska rupia",
    ILS: "Izraelský šekel",
    INR: "Indická rupia",
    KRW: "Juhokórejský won",
    MXN: "Mexické peso",
    MYR: "Malajzijský ringgit",
    NZD: "Novozélandský dolár",
    PHP: "Filipínske peso",
    SGD: "Singapurský dolár",
    THB: "Thajský baht",
    ZAR: "Juhoafrický rand",
  };
}

export const isIphoneX = () => {
  if (Platform.OS === "ios") {
    return height === 812 || width === 812;
  } else {
    return false;
  }
};

const Container = styled.SafeAreaView`
  background-color: #fff;
  flex: 1;
`;

const CurrencyRow = styled.View`
  flex-direction: row;
  margin-bottom: ${isPad ? 0 : 20}px;
`;

const FlagCol = styled.View`
  flex: 4;
  justify-content: center;
`;

const FlagContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

const FlagImg = styled.Image`
  resize-mode: cover;
  width: ${(width * 55) / 375}px;
  height: ${(width * 35) / 375}px;
  border-width: 1px;
  border-color: rgba(255, 255, 255, 0.2);
`;

const CurrencyCode = styled.Text`
  color: #fff;
  margin-left: ${(width * 10) / 375}px;
  font-size: ${width * 0.05};
  font-weight: 600;
`;

const DropdownArrow = styled.Image`
  resize-mode: contain;
  height: 12px;
  width: 12px;
  margin: 0 7px;
`;

const InputCol = styled.View`
  flex: 6;
  height: 52px;
  justify-content: center;
`;

const CurrencyNumber = styled.Text`
  font-size: 40px;
  color: #fff;
  border-color: #eee;
  border-width: 0;
  width: 100%;
  text-align: right;
  padding: 2px 0px;
`;

const DividerRow = styled.View`
  flex-direction: row;
  margin-bottom: 20px;
`;
const HorizontalLine = styled.View`
  background-color: #fff;
  height: 1px;
  flex: 1;
  margin-top: ${isPad ? 20 : 25}px;
  margin-right: ${isPad ? 15 : 20}px;
`;
const SwapIcon = styled.View`
  background-color: #fff;
  border-radius: 50px;
  height: ${isPad ? 40 : 50}px;
  width: ${isPad ? 40 : 50}px;
  padding: ${isPad ? 10 : 12}px;
  box-shadow: 0 0 4px rgba(40, 40, 40, 0.3);
  elevation: 8;
`;

const PickerRow = styled.View`
  flex-direction: row;
`;

const ArrowIcon = styled.Image`
  resize-mode: contain;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-21px, -21px);
  height: 42px;
  width: 42px;
`;

const PickerColumn = styled.View`
  flex: 1;
`;

const KeypadRow = styled.View`
  flex-direction: row;
  flex: 1;
`;

const KeypadTouchable = isAndroid
  ? styled.TouchableNativeFeedback``
  : styled.TouchableHighlight.attrs({
      underlayColor: "#fff",
      activeOpacity: 0.4,
    })``;

const KeypadButton = styled.View`
  width: ${width / 3 - 10}px;
  flex: 1;
  margin: 5px;
  align-items: center;
  justify-content: center;
  background: #fff;
  padding: 5px 0;
  border-radius: 10px;
  box-shadow: 0 1px 3px rgba(179, 179, 200, 0.6);
  border-color: #eee;
  border-width: ${isAndroid ? 1 : 0}px;
  elevation: 4;
`;

const KeypadText = styled.Text`
  font-weight: 300;
  font-size: 28px;
`;

const KeypadBackspace = styled.Image`
  resize-mode: contain;
  height: 22px;
  width: 22px;
`;

const DateText = styled.Text`
  color: #fff;
  width: 100%;
  font-size: ${isAndroid ? 13 : 11}px;
  text-align: center;
`;
